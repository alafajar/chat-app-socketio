//metode buat konsumsi kiriman data server
const socket = io()

const $messageForm        = document.querySelector('#message-form');
const $messageFormInput   = $messageForm.querySelector('input');
const $messageFormButton  = $messageForm.querySelector('button');
const $sendLocationButton = document.querySelector('#send-location');
const $messages = document.querySelector('#messages')


// Templates
const messageTemplate         = document.querySelector('#message-template').innerHTML
const locationMessageTemplate = document.querySelector('#location-message-template').innerHTML

socket.on('message', (message) => {
	console.log(message);
	const html = Mustache.render(messageTemplate, {
		message
	});
	$messages.insertAdjacentHTML('beforeend', html)
})

socket.on('locationMessage', (url) => {
	const html = Mustache.render(locationMessageTemplate, {
		url
	})
	$messages.insertAdjacentHTML('beforeend', html)
	
})

document.querySelector('#message-form').addEventListener('submit', (e) => {
	e.preventDefault()
	//disable
	$messageFormButton.setAttribute('disabled', 'disabled');

	const message = e.target.elements.message.value 

	socket.emit('sendMessage', message, (error) => {

		//enable
		$messageFormButton.removeAttribute('disabled');
		$messageFormInput.value = ''
		$messageFormInput.focus()

		if(error) {
			return console.log(error);
		}
		console.log('Message delivered!');
	})
})

// document.querySelector('#increment').addEventListener('click', () =>{
// 	console.log('Clicked');
// })

$sendLocationButton.addEventListener('click', () => {
	//navigator bawaan browser
	if(!navigator.geolocation) {
		return alert('Geolocation is not supported by your browser')
	}

	$sendLocationButton.setAttribute('disabled', 'disabled')

	navigator.geolocation.getCurrentPosition( (position) => {
		socket.emit('sendLocation', {
			latitude: position.coords.latitude,
			longitude: position.coords.longitude,
		}, () => {
			$sendLocationButton.removeAttribute('disabled')
			console.log('Location shared!');
		})
	})
})